package com.spring_learning.spring_learning.exc2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DummyLogger {
    public void sayHello(String text) {
        log.info(text);
    }
}